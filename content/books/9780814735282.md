+++
title = "Charles Dickens and the Image of Women"
author = "David K. Holbrook"
citation_link = "http://hdl.handle.net/2333.1/xd25496n"
description = "How successful is Dickens in his portrayal of women? Dickens has been represented (along with William Blake and D.H. Lawrence) as one who championed the life of the emotions often associated with the \"feminine.\" Yet some of his most important heroines are totally submissive and docile. Dickens, of course, had to accept the conventions of his time. It is obvious, argues Holbrook, that Dickens idealized the father-daughter relationship, and indeed, any such relationship that was unsexual, like that of Tom Pinch and his sister&#151;but why? Why, for example, is the image of woman so often associated with death, as in Great Expectations? Dickens's own struggles over relationships with women have been documented, but much less has been said about the unconscious elements behind these problems. Using recent developements in psychoanalytic object-relations theory, David Holbrook offers new insight into the way in which the novels of Dickens&#151;particularly Bleak House, Little Dorrit, and Great Expectations&#151;both uphold emotional needs and at the same time represent the limits of his view of women and that of his time."
description_html = "<p>How successful is Dickens in his portrayal of women? Dickens has been represented (along with William Blake and D.H. Lawrence) as one who championed the life of the emotions often associated with the \"feminine.\" Yet some of his most important heroines are totally submissive and docile.</p> <p>Dickens, of course, had to accept the conventions of his time. It is obvious, argues Holbrook, that Dickens idealized the father-daughter relationship, and indeed, any such relationship that was unsexual, like that of Tom Pinch and his sister&#151;but why? Why, for example, is the image of woman so often associated with death, as in <I>Great Expectations</I>? Dickens's own struggles over relationships with women have been documented, but much less has been said about the unconscious elements behind these problems.</p> <p>Using recent developements in psychoanalytic object-relations theory, David Holbrook offers new insight into the way in which the novels of Dickens&#151;particularly <i>Bleak House</i>, <i>Little Dorrit</i>, and <i>Great Expectations</I>&#151;both uphold emotional needs and at the same time represent the limits of his view of women and that of his time.</p>"
format = "208 Pages"
publisher = "New York University Press"
date = "1993"
thumbHref = "epub_content/9780814735282/ops/images/9780814735282-th.jpg"
subjects = ["LITERARY", "English, Irish, Scottish, Welsh"]
isbn = "9780814735282"

+++
