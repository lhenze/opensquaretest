+++
title = "Deconstruction Is/In America"
subtitle = "A New Sense of the Political"
author = "Anselm Haverkamp, H. R. Dodge"
citation_link = "http://hdl.handle.net/2333.1/k3j9kfwk"
description = "What impact has deconstruction had on the way we read American culture? And how is American culture itself peculiarly deconstructive? To address these questions, this volume brings together some of the most provocative thinkers associated with deconstruction, among them Jacques Derrida, Judith Butler, and Avital Ronnel. Ranging across a wide field, from the ethics of reading to the rhetoric of performance, the contributors offer provocative insights into a new sense of the political. The America of the volume's title turns out to be the place where the politics and poetics of responsibility meet. It is also the place where we confront the tension between difference and profound otherness. "
description_html = "<p>What impact has deconstruction had on the way we read American culture? And how is American culture itself peculiarly deconstructive?</p> <p>To address these questions, this volume brings together some of the most provocative thinkers associated with deconstruction, among them Jacques Derrida, Judith Butler, and Avital Ronnel. Ranging across a wide field, from the ethics of reading to the rhetoric of performance, the contributors offer provocative insights into a new sense of the political. The America of the volume's title turns out to be the place where the politics and poetics of responsibility meet. It is also the place where we confront the tension between difference and profound otherness. </p>"
format = "274 Pages"
publisher = "New York University Press"
date = "1995"
thumbHref = "epub_content/9780814735190/ops/images/9780814735190-th.jpg"
subjects = ["POLITICAL", "Essays"]
isbn = "9780814735190"

+++
