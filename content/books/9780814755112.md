+++
title = "Postmodern Legal Movements"
subtitle = "Law and Jurisprudence At Century's End"
author = "Gary Minda"
citation_link = "http://hdl.handle.net/2333.1/z612jp24"
description = "What do Catharine MacKinnon, the legacy of Brown v. Board of Education, and Lani Guinier have in common? All have, in recent years, become flashpoints for different approaches to legal reform. In the last quarter century, the study and practice of law have been profoundly influenced by a number of powerful new movements; academics and activists alike are rethinking the interaction between law and society, focusing more on the tangible effects of law on human lives than on its procedural elements. In this wide-ranging and comprehensive volume, Gary Minda surveys the current state of legal scholarship and activism, providing an indispensable guide to the evolution of law in America."
description_html = "<p>What do Catharine MacKinnon, the legacy of Brown v. Board of Education, and Lani Guinier have in common? All have, in recent years, become flashpoints for different approaches to legal reform. In the last quarter century, the study and practice of law have been profoundly influenced by a number of powerful new movements; academics and activists alike are rethinking the interaction between law and society, focusing more on the tangible effects of law on human lives than on its procedural elements.</p> <p>In this wide-ranging and comprehensive volume, Gary Minda surveys the current state of legal scholarship and activism, providing an indispensable guide to the evolution of law in America.</p>"
format = "362 Pages"
publisher = "New York University Press"
date = "1995"
thumbHref = "epub_content/9780814755112/ops/images/9780814755112-th.jpg"
subjects = ["LAW", "General"]
isbn = "9780814755112"

+++
